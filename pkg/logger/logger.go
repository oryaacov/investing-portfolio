package logger

import (
	"fmt"

	"github.com/oryaacov/lumberjack"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

//LogConfig represent the logger file paths, rolling configuration, and verbosity.
type LogConfig struct {
	TrafficLogFile string `json:"trafficLogFile"`
	LogFilePath    string `json:"logFilePath"`
	MaxLogSize     int    `json:"maxLogSize"`
	LogLevel       string `json:"logLevel"`
	MaxLogRolling  int    `json:"maxLogRolling"`
}

//Logger is a multi logging utility, can be used for auditing writing diffrenet log files
//or use diffrenet logging libraries without breaking the code.
type Logger struct {
	//use a diffrent logger to print data to console as well. very userful during development.
	//shouldn't be user at production since writing to the stdio is userless and costy.
	consoleLogger *zap.Logger
	//main rolling file logger (using lumberjack)
	Log *zap.Logger
	//traffic logger that will be used by Gin
	TrafficLogger *zap.Logger

	printToConsole bool
}

//Init the logger
func (l *Logger) Init(conf LogConfig, printToConsole bool) error {
	var err error
	l.printToConsole = printToConsole
	if !l.printToConsole {
		if err = l.initConsoleLogger(); err != nil {
			return err
		}
	}
	if l.Log, err = initZapWithLumberjack(conf.LogFilePath, &conf); err != nil {
		return err
	}
	if l.TrafficLogger, err = initZapWithLumberjack(conf.TrafficLogFile, &conf); err != nil {
		return err
	}
	return nil
}

func (l *Logger) initConsoleLogger() error {
	var err error
	if l.consoleLogger, err = zap.NewDevelopment(); err != nil {
		return err
	}
	return nil
}

func createLumberJackConfigLogger(logPath string, conf *LogConfig) *lumberjack.Logger {
	return &lumberjack.Logger{
		Filename:   logPath,
		MaxSize:    conf.MaxLogSize,
		MaxBackups: conf.MaxLogRolling,
		MaxAge:     28, // days
	}
}

func initZapWithLumberjack(logPath string, conf *LogConfig) (*zap.Logger, error) {
	w := zapcore.AddSync(createLumberJackConfigLogger(logPath, conf))
	zapConf := zap.NewProductionEncoderConfig()
	zapConf.EncodeTime = zapcore.RFC3339TimeEncoder
	zapLevel, err := stringToZapLevel(conf.LogLevel)
	if err != nil {
		return nil, err
	}
	core := zapcore.NewCore(
		zapcore.NewJSONEncoder(zapConf),
		w,
		zapLevel,
	)
	return zap.New(core), nil
}

func stringToZapLevel(level string) (zapcore.Level, error) {
	switch level {
	case "debug":
		return zap.DebugLevel, nil
	case "info":
		return zap.InfoLevel, nil
	case "warn":
		return zap.WarnLevel, nil
	case "error":
		return zap.ErrorLevel, nil
	case "dpanic":
		return zap.DPanicLevel, nil
	case "panic":
		return zap.PanicLevel, nil
	case "fatal":
		return zap.FatalLevel, nil
	default:
		return zap.FatalLevel, fmt.Errorf("invalid log level %s", level)
	}
}

//LogInfo log an info message
func (l *Logger) LogInfo(format string, a ...interface{}) {
	msg := fmt.Sprintf(format, a...)
	l.Log.Info(msg)
	if !l.printToConsole {
		l.consoleLogger.Info(msg)
	}
}

//LogDebug log an debeug message
func (l *Logger) LogDebug(format string, a ...interface{}) {
	msg := fmt.Sprintf(format, a...)
	l.Log.Debug(msg)
	if !l.printToConsole {
		l.consoleLogger.Debug(msg)
	}
}

//LogWarn log an info message
func (l *Logger) LogWarn(format string, a ...interface{}) {
	msg := fmt.Sprintf(format, a...)
	l.Log.Warn(msg)
	if !l.printToConsole {
		l.consoleLogger.Warn(msg)
	}
}

//LogError log an error with a message
func (l *Logger) LogError(msg string, err error) {
	l.Log.Error(msg, zap.Error(err))
	if !l.printToConsole {
		l.consoleLogger.Error(msg, zap.Error(err))
	}
}

//LogPanic log panic with any args with springln
func (l *Logger) LogPanic(args ...interface{}) {
	l.Log.Panic(fmt.Sprintln(args...))
}
