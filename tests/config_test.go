package tests

import (
	"testing"

	"bitbucket.org/oryaacov/investing-portfolio/internal/config"
)

//check the configuration file.
//usually for more complex testing I would of use a library as testify
//I'll skip the rest of the required testing since this is just a demo and not a real project.
func TestConfigurationFile(t *testing.T) {
	conf, err := config.FromFile("./mocks/inv-mock.json")
	if err != nil {
		t.Error(err)
		t.Fail()
	}
	if conf == nil || len(conf.App.Name) == 0 {
		t.Error("failed to read data from configuration file")
		t.Fail()
	}
	conf, err = config.FromFile("bad file path")
	if err == nil || conf != nil {
		t.Error("should have failed on bad configuration path")
		t.Fail()
	}
}
