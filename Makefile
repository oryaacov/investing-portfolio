APP_NAME=investing-portfolio
CONF=inv-portfolio-dev.json
build:  
	@go test $(CURDIR)/tests
	@go build -o $(CURDIR)/$(APP_NAME) $(CURDIR)/cmd/$(APP_NAME)/main.go

run: build 
	@ $(CURDIR)/$(APP_NAME) $(CURDIR)/configs/${CONF}

build-docker:
	@docker build -t $(APP_NAME) -f dev.dockerfile .

run-docker:
	docker run --network="host" -it --rm -v `pwd`:/app --name $(APP_NAME) $(APP_NAME)
