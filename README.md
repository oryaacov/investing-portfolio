# Enviroment  
---
 - make sure that you have mysql-server installed (latest)
 - make sure that you have go 1.11+ (go 1.13+ is preferd)
 - make sure that you have docker installed
 - make sure that you have npm installed (And node)
 - please execute  /migrations/1_1_0_0_investing_portfolio.up.sql migration file before running the application 

# Build
--- 
## Frontend 
    $ cd website
    $ npm install
    $ npm run env {{enviroment file name}} //$npm run env .dev-env
    $ npm run build //will generate the website at /website/dist 
    
## Backend
### build locally
    $ make build
### build docker  
    $ make build-docker
 (sudo is usually required in order to build and run a new container)


# RUN
___
## Backend

### run locally
    $ make run
    or
    $ ./investing-portfolio {{configuration_file}}
### run in docker  
    $ make run-docker

### site: http://{{server_url:port_number}}/site 
### in my enviroment: http://localhost:8080/site

