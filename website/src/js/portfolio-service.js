import { DOMUtils, HTTPUtils } from "./utils";

const HTTP_GET = "GET";
const HTTP_DELETE = "DELETE";
const HTTP_POST = "POST";
export class PortfolioService {

    _allInstruments;
    _userInstruments;
    static instance;
    constructor() {
        PortfolioService.instance = this;
    }


    getAllInstruments = (callback) => {
        HTTPUtils.sendHTTP(HTTP_GET, `${process.env.BASE_URL}/instrument`,
            { token: process.env.USER_TOKEN }
            , undefined, callback, (f) => { console.error(f); });
    }

    getUserInstruments = (callback) => {
        HTTPUtils.sendHTTP(HTTP_GET, `${process.env.BASE_URL}/user/${process.env.HARD_CODED_USERNAME}/instrument`,
            { token: process.env.USER_TOKEN }, undefined, callback
            , (f) => { console.error(f); });
    }



    handleGetUserInstrumentsResponse = (response) => {
        this._userInstruments = response;
    }

    handleGetAllInstrumentsResponse = (response) => {
        this._allInstruments = response;
    }

    removeInstrument = (item) => {
        HTTPUtils.sendHTTP(HTTP_DELETE, `${process.env.BASE_URL}/user/${process.env.HARD_CODED_USERNAME}/instrument`,
            { token: process.env.USER_TOKEN }, PortfolioService._toInstrumentJson(item), () => { DOMUtils.toastrSuccess("item deleted successfully!"); },
            (f) => { console.error(f); });
    }
    addInstrument = (item, callback) => {
        HTTPUtils.sendHTTP(HTTP_POST, `${process.env.BASE_URL}/user/${process.env.HARD_CODED_USERNAME}/instrument`,
            { token: process.env.USER_TOKEN }, PortfolioService._toInstrumentJson(item), () => {
                callback();
            },
            (f) => {
                if (f.status === 409) {
                    console.error("409");

                    DOMUtils.toastWarning("item already exists");
                }
                console.error(f);
            });

    }

    static _toInstrumentJson = (item) => {
        if (item && item.length === 4) {
            return JSON.stringify({ id: item[0], name: item[1], symbol: item[2], type: item[3] });
        }
        return null;
    }

    get allInstruments() {
        return this._allInstruments;
    }

    get userInstruments() {
        return this._userInstruments;
    }
}
export const portfolioService = new PortfolioService();




