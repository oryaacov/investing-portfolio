import "popper.js";
import "bootstrap";
import { DOMUtils } from "./utils";
import { portfolioService, PortfolioService } from "./portfolio-service";
import { myInstrumentsDataTable, allInstrumentsDataTable } from "./datatable";

const init = () => {
  //init the toaster
  DOMUtils.initToastr();
  //init the datatables action buttons (add item for all instrument table and remove item for my instruments )
  initDataTablesButtonFuncs();
  //get all instruments, note since there are only few records I used here a front-end based seach
  //I implemented a backend search as well (can be used with postman/curl)
  portfolioService.getAllInstruments((s) => {
    PortfolioService.instance.handleGetAllInstrumentsResponse(s);
    allInstrumentsDataTable.initTable(s);
  });
  //get user instruments
  portfolioService.getUserInstruments((s) => {
    PortfolioService.instance.handleGetUserInstrumentsResponse(s);
    myInstrumentsDataTable.initTable(s);
  });
};

//used here since I wanted to import everything here and not play with dependencies at the datatable
//file
const initDataTablesButtonFuncs = () => {
  //handle delete evernt
  myInstrumentsDataTable._setButtonFunction(function () {
    myInstrumentsDataTable.deleteRow($(this));
  });

  //handle add item  to my list event
  allInstrumentsDataTable._setButtonFunction(function () {
    var data = allInstrumentsDataTable.table.row($(this).parents("tr")).data();
    portfolioService.addInstrument(data, () => {
      myInstrumentsDataTable.addRow(data);  
      DOMUtils.toastrSuccess("Instrument added successfully");
    });
  });
};


//calls init (starting point)
init();
