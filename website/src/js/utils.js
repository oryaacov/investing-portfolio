import * as toastr from "toastr";

export class HTTPUtils {

    static sendHTTP(method, url, headers, urlParams, successCallback,failedCallback) {
        $.ajax({
            headers: headers,
            data: urlParams,
            async: false,
            contentType:"application/json",
            type: method,
            url: url,
        }).done(data => {
            successCallback(data);
        }
        ).fail(function (data) {
           if (failedCallback){
            failedCallback(data);
           }else{
               console.log(data);
           }
        });
    }
   
}

export class DOMUtils {
    static initToastr = () => {
        toastr.options.positionClass = "toast-bottom-left";
    }

    static toastrSuccess = (msg) => {
        toastr.success(msg);
    }
    static toastWarning = (msg) => {
        toastr.warning(msg);
    }
    static setElementInnerHTML = (elm, html) => {
        elm.html(html);
    };

    static setElementInnerText = (elm, text) => {
        elm.text(text);
    };
}



