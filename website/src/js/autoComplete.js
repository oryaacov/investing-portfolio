import * as autocomplete from "autocompleter";
class AutoComplete {
    autoCompleteElement;
    data;
    static instance;
    constructor(autoCompleteElementID) {
        this.autoCompleteElement = document.getElementById(autoCompleteElementID);
        AutoComplete.instance=this;
    }

    initAutoComplete(data) {
        this.data=data;
        autocomplete({
            input: this.autoCompleteElement,
            fetch: function (text, update) {
                text = text.toLowerCase();
                // you can also use AJAX requests instead of preloaded data
                var suggestions = AutoComplete.instance.data.filter(n => n.label.toLowerCase().startsWith(text))
                update(suggestions);
            },
            onSelect: function (item) {
                input.value = item.label;
            }
        });
    }

}
export const autoComplete = new AutoComplete("inv-autocomplete");