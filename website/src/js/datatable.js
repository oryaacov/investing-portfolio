import "datatables";
import { portfolioService } from "./portfolio-service";

class DataTable {
    tableElement;
    tableID;
    table;
    buttonFunc;
    buttonHTML;
    constructor(tableID, buttonHTML) {
        this.tableID = tableID;
        this.tableElement = $(`#${tableID}`);
        this.buttonHTML = buttonHTML;
    }

    _setButtonFunction(buttonFunc) {
        this.buttonFunc = buttonFunc;
    }

    _toTwoDArray(dataset) {
        const res = [];
        for (let i = 0; i < dataset.length; i++) {
            res.push([dataset[i].id, dataset[i].name, dataset[i].symbol, dataset[i].type]);
        }
        return res;
    }

    initTable = (data) => {
        const dataset = this._toTwoDArray(data);

        this.table = this.tableElement.DataTable({
            data: dataset,
            dom: "Bfrtip",
            select: true,
            columns: [
                { title: "ID" },
                { title: "Name" },
                { title: "Symbol" },
                { title: "Type" },
                { title: "Action" }],
            columnDefs: [{
                "targets": -1,
                "data": null,
                "defaultContent": this.buttonHTML
            }]

        });

        $(`#${this.tableID} tbody`).on("click", "button", this.buttonFunc);

    }
    addRow = (data) => {
        var rowNode = this.table
            .row.add(data)
            .draw()
            .node();

        $(rowNode)
            .animate({ color: "black" });
    }
    deleteRow = (item) => {
        var data = myInstrumentsDataTable.table.row(item.parents("tr")).data();
        portfolioService.removeInstrument(data);
        myInstrumentsDataTable.table.row(item.parents("tr"))
            .remove()
            .draw();
    }

}
export const myInstrumentsDataTable = new DataTable("my-instruments-table", "<button>remove</button>");
export const allInstrumentsDataTable = new DataTable("all-instruments-table", "<button>add to list</button>");

