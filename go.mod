module bitbucket.org/oryaacov/investing-portfolio

go 1.13

require (
	github.com/gin-contrib/zap v0.0.1
	github.com/gin-gonic/gin v1.6.3
	github.com/go-sql-driver/mysql v1.5.0
	github.com/itsjamie/gin-cors v0.0.0-20160420130702-97b4a9da7933
	github.com/jmoiron/sqlx v1.2.0
	github.com/oryaacov/lumberjack v2.0.1+incompatible
	go.uber.org/zap v1.16.0
)
