CREATE TABLE instrument(
instrumentId INTEGER NOT NULL PRIMARY KEY
,name
VARCHAR(41) NOT NULL
,symbol
VARCHAR(7) NOT NULL
,instrumentType VARCHAR(9) NOT NULL
);

INSERT INTO instrument(instrumentId,name,symbol,instrumentType) VALUES
(1,'Euro US Dollar','EUR/USD','currency'),
(10,'Euro Swiss Franc','EUR/CHF','currency'),
(9,'Euro Japanese Yen','EUR/JPY','currency'),
(956731,'Investing.com Euro Index','inveur','indice'),
(2124,'US Dollar Euro','USD/EUR','currency'),
(976573,'Sygnia Itrix Euro Stoxx 50 ETF','SYGEUJ','etf'),
(997393,'NewWave EUR Currency Exchange Traded Note','NEWEURJ','etf'),
(998227,'Diesel European Gasoil Futures','DSEL1c1','commodity'),
(175,'Euro Stoxx 50','STOXX50','indice'),
(15978,'Euronet Worldwide Inc','EEFT','equities'),
(6,'Euro British Pound','EUR/GBP','currency'),
(15,'Euro Australian Dollar','EUR/AUD','currency'),
(16,'Euro Canadian Dollar','EUR/CAD','currency'),
(52,'Euro New Zealand Dollar','EUR/NZD','currency'),
(1487,'Australian Dollar Euro','AUD/EUR','currency'),
(1525,'Canadian Dollar Euro','CAD/EUR','currency');

CREATE TABLE investing_portfolio.`user` (
	id INT NOT NULL AUTO_INCREMENT,
	username varchar(100) NOT NULL,
	email varchar(300) NOT NULL,
    token varchar(38) NULL,
	created_date TIMESTAMP NOT NULL,
	token_exp TIMESTAMP NULL,
	last_login TIMESTAMP NULL,
	password varchar(100) NOT NULL,
	CONSTRAINT user_PK PRIMARY KEY (id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;

INSERT INTO investing_portfolio.`user` (username,email,created_date,last_login,password,token,token_exp)
	VALUES ('testuser','myemail@gmail.com',NOW(),NOW(),'my-encrypted-password','f865e8e2-3eff-4915-9244-8b367f670091',date("2021-12-31 10:00:00"));

CREATE TABLE investing_portfolio.user_instrument (
	id INT NOT NULL AUTO_INCREMENT,
	user_id int NOT NULL,
	instrument_id INT NOT NULL,
	created_at TIMESTAMP NOT NULL,
	deleted_at TIMESTAMP NULL,
	CONSTRAINT user_instrument_PK PRIMARY KEY (id),
	CONSTRAINT user_instrument_FK FOREIGN KEY (instrument_id) REFERENCES investing_portfolio.instrument(instrumentId),
	CONSTRAINT user_instrument_FK_1 FOREIGN KEY (user_id) REFERENCES investing_portfolio.`user`(id)
)
ENGINE=InnoDB
DEFAULT CHARSET=utf8
COLLATE=utf8_general_ci;


GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY 'temp-password';