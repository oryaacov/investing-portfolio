package main

import (
	"fmt"
	"os"

	"bitbucket.org/oryaacov/investing-portfolio/internal/config"
	"bitbucket.org/oryaacov/investing-portfolio/internal/dal"
	"bitbucket.org/oryaacov/investing-portfolio/internal/server"
	"bitbucket.org/oryaacov/investing-portfolio/pkg/logger"
)

func main() {
	if len(os.Args) <= 1 {
		fmt.Printf("USAGE:%s {{configuration file path}}", os.Args[0])
		return
	}
	var err error
	fmt.Println("reading configuration file...")
	configuration, err := config.FromFile(os.Args[1])
	if err != nil {
		fmt.Println("failed to load configuration file")
		return
	}
	fmt.Println("done!\ninit logger...")
	log := &logger.Logger{}
	if err = log.Init(configuration.Log, configuration.App.Enviroment == "prod"); err != nil {
		fmt.Println("failed to init logger")
		return
	}
	log.LogInfo("done!\nopenning db connection...")
	db, err := dal.OpenDBConnection(configuration.DB.Database, configuration.DB.ConnectionString)
	if err != nil {
		log.LogError("failed to open db connection", err)
		return
	}
	log.LogInfo("done!\ninit %s %s version %s server...", configuration.App.Name, configuration.App.Enviroment, configuration.App.Version)
	serv, err := server.InitServer(*configuration, db, log)
	if err != nil {
		log.LogError("failed to start server", err)
		return
	}
	serv.Start()
}
