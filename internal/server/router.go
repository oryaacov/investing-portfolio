package server

import (
	"net/http"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	cors "github.com/itsjamie/gin-cors"
)

//buildRouter inits the gin with the configured CORS policy and logger
func (s *server) buildRouter() {
	router := gin.New()
	router.Use(cors.Middleware(cors.Config{
		Origins:         s.config.HTTPServer.AllowedOrigins,
		Methods:         s.config.HTTPServer.AllowedMethods,
		RequestHeaders:  s.config.HTTPServer.AllowedHeaders,
		ExposedHeaders:  "",
		MaxAge:          50 * time.Second,
		Credentials:     true,
		ValidateHeaders: false,
	}))
	router.Use(gin.Recovery())
	router.Use(ginzap.Ginzap(s.log.TrafficLogger, time.RFC3339, true))
	s.router = router
}

func (s *server) registerRoutes() {
	api := s.router.Group("/api")
	user := api.Group("/user/:username")
	api.Use(s.verifyToken())
	user.GET("/instrument", s.getUserInstruments)
	user.PUT("/instrument", s.updateUserInstruments)
	user.POST("/instrument", s.addToUserInstruments)
	user.DELETE("/instrument", s.RemoveFromUserInstruments)
	api.GET("/instrument", s.getAllInstruments)
	s.router.StaticFS("/site", http.Dir(s.config.HTTPServer.StaticFilesLocation))

}
