package server

import (
	"errors"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

func (s *server) verifyToken() gin.HandlerFunc {
	return func(c *gin.Context) {
		token := c.GetHeader("token")
		user, err := s.getUserByToken(token)
		if err != nil || user == nil {
			if err != nil {
				s.log.LogError("failed query user by token", err)
			}
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}
		if user.TokenExp.Valid && time.Now().Local().After(user.TokenExp.Time) {
			c.AbortWithError(http.StatusUnauthorized, errors.New("expired token, please re-login"))
			return
		}
		s.log.Log.Info("valid token")
	}
}
