package server

import (
	"fmt"

	"bitbucket.org/oryaacov/investing-portfolio/internal/app"
	"bitbucket.org/oryaacov/investing-portfolio/internal/config"
	"bitbucket.org/oryaacov/investing-portfolio/pkg/logger"
	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
)

//Server is the main server struct, containing the db,logger,config and router together
type server struct {
	config config.Configuration
	db     *sqlx.DB
	log    *logger.Logger
	router *gin.Engine
	//the user cache would usually be managed diffrently, with record experation, size limit or by 3-party
	//tool as redis
	userCache map[string]*app.User
}

var serverInstance *server = nil

//InitServer creates a singelton server instance,build the router, initialize the db,log and configuration files
func InitServer(config config.Configuration, db *sqlx.DB, log *logger.Logger) (*server, error) {
	if serverInstance == nil {
		serverInstance = &server{config: config, db: db, log: log}
		serverInstance.buildRouter()
		serverInstance.registerRoutes()
		serverInstance.userCache = make(map[string]*app.User)
	}
	return serverInstance, nil
}

//Start the http server
func (s *server) Start() {
	defer s.db.Close()
	s.router.Run(fmt.Sprintf("%s:%d", s.config.HTTPServer.BaseURL, s.config.HTTPServer.Port))
}

func (s *server) getUserByToken(token string) (*app.User, error) {
	if user, ok := s.userCache[token]; ok {
		return user, nil
	}
	user, err := app.GetUserByToken(token, s.db)
	if err != nil {
		return nil, err
	}
	if user != nil {
		s.userCache[token] = user
	}
	return user, nil
}
