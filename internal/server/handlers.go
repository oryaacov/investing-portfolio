package server

import (
	"encoding/json"
	"net/http"
	"strings"

	"bitbucket.org/oryaacov/investing-portfolio/internal/app"
	"github.com/gin-gonic/gin"
)

//GetServerVersion return server version
func (s *server) getServerVersion(c *gin.Context) {
	c.Data(http.StatusOK, "text/plain", []byte(s.config.App.Version))
}

func (s *server) getUserInstruments(c *gin.Context) {
	user := s.isValidUserQuery(c)
	if user == nil {
		return
	}
	instruments, err := user.GetUserInstruments(s.db)
	if err != nil {
		s.log.LogError("failed to get user instruments", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return
	}
	if instruments == nil || len(instruments) == 0 {
		c.Status(202)
		return
	}
	c.JSON(http.StatusOK, instruments)
}

func (s *server) updateUserInstruments(c *gin.Context) {
	user := s.isValidUserQuery(c)
	if user == nil {
		return
	}
	var request app.UpdateUserInstrumentsRequest
	if !s.readBody(c, &request) {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	res, err := user.UpdateUserInstruments(request.Instruments, s.db)
	if err != nil {
		s.internalServerError(err, c)
		return
	}
	c.JSON(http.StatusOK, res)
}

func (s *server) addToUserInstruments(c *gin.Context) {
	user := s.isValidUserQuery(c)
	if user == nil {
		return
	}
	var instrument app.Instrument
	if !s.readBody(c, &instrument) {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	alreadyExists, err := user.AddToUserInstrumens(instrument, s.db)
	if err != nil {
		s.internalServerError(err, c)
		return
	}
	if alreadyExists {
		c.Status(http.StatusConflict)
		return
	}
	c.Status(http.StatusOK)
}

func (s *server) RemoveFromUserInstruments(c *gin.Context) {
	user := s.isValidUserQuery(c)
	if user == nil {
		return
	}
	var instrument app.Instrument
	if !s.readBody(c, &instrument) {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	err := user.RemoveFromUserInstrument(instrument, s.db)
	if err != nil {
		s.internalServerError(err, c)
		return
	}
	c.Status(http.StatusOK)
}

//GetServerVersion return server version
func (s *server) getAllInstruments(c *gin.Context) {

	var instruments []app.Instrument
	var err error
	query := c.Request.URL.Query().Get("q")
	if len(query) == 0 {
		instruments, err = app.GetAllInstruments(s.db)
	} else {
		instruments, err = app.GetInstrumentsByQuery(query, s.db)
	}
	if err != nil {
		s.internalServerError(err, c)
		return
	}
	c.JSON(http.StatusOK, instruments)
}

func (s *server) internalServerError(err error, c *gin.Context) {
	s.log.LogError("failed to get all instruments", err)
	c.Status(http.StatusInternalServerError)
}

func (s *server) readBody(c *gin.Context, req interface{}) bool {
	var err error
	var body []byte
	if body, err = c.GetRawData(); err != nil {
		s.log.LogError("failed to parse body", err)
		return false
	}
	if err = json.Unmarshal(body, req); err != nil {
		s.log.LogError("failed to unmarshel body", err)
		return false
	}
	return true
}

func (s *server) isValidUserQuery(c *gin.Context) *app.User {
	user, err := s.getUserByToken(c.GetHeader("token"))
	if err != nil {
		s.log.LogError("failed to get user", err)
		c.AbortWithStatus(http.StatusInternalServerError)
		return nil
	}
	if !strings.EqualFold(user.Username, c.Param("username")) {
		s.log.LogWarn("username %s doesn't match the user token", c.Param("username"))
		c.AbortWithStatus(http.StatusBadRequest)
		return nil
	}
	return user
}
