package app

import "github.com/jmoiron/sqlx"

//Instrument contains symbol and type
type Instrument struct {
	ID     int64  `db:"instrumentId" json:"id"`
	Name   string `db:"name" json:"name"`
	Symbol string `db:"symbol" json:"symbol"`
	Type   string `db:"instrumentType" json:"type"`
}

//GetAllInstruments Query all of the instruments from the db.
//usually pagination kind of logic would have been applied
func GetAllInstruments(db *sqlx.DB) ([]Instrument, error) {
	var res []Instrument
	if err := db.Select(&res, "SELECT * FROM instrument"); err != nil {
		return nil, err
	}
	return res, nil
}

//GetInstrumentsByQuery searching the instruments name&symbol for a specific string
//of instruments justify simple sql query search
//usually pagination kind of logic would have been applied
func GetInstrumentsByQuery(q string, db *sqlx.DB) ([]Instrument, error) {
	var res []Instrument
	if err := db.Select(&res, "SELECT * FROM instrument where name like CONCAT('%', ?, '%') or symbol like CONCAT('%', ?, '%');", q, q); err != nil {
		return nil, err
	}
	return res, nil
}
