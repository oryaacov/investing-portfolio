package app

import (
	"database/sql"
	"time"

	"bitbucket.org/oryaacov/investing-portfolio/internal/dal"
	"github.com/jmoiron/sqlx"
)

//User token based user
type User struct {
	ID          int64          `db:"id"`
	Username    string         `db:"username"`
	Email       string         `db:"email"`
	Token       sql.NullString `db:"token"`
	CreatedDate time.Time      `db:"created_date"`
	TokenExp    sql.NullTime   `db:"token_exp"`
	LastLogin   sql.NullTime   `db:"last_login"`
	Password    string         `db:"password"`
}

//UpdateUserInstrumentsRequest contains a list of the new instrument that the user will have
type UpdateUserInstrumentsRequest struct {
	Instruments []Instrument `json:"instruments"`
}

//GetUserByToken query a user by token
func GetUserByToken(token string, db *sqlx.DB) (*User, error) {
	var res User
	if err := db.Get(&res, "SELECT * FROM user WHERE token=?;", token); err != nil {
		if dal.IsEmptyResult(err) {
			return nil, nil
		}
		return nil, err
	}
	return &res, nil
}

func (u *User) isInstrumentExists(instrument Instrument, db *sqlx.DB) (bool, error) {
	var res bool
	if err := db.Get(&res, "SELECT EXISTS(SELECT 1 FROM user_instrument WHERE  user_id=? and instrument_id = ? and deleted_at is null);", u.ID, instrument.ID); err != nil {
		return false, err
	}
	return res, nil
}

//AddToUserInstrumens add an instrument into the user list
func (u *User) AddToUserInstrumens(instrument Instrument, db *sqlx.DB) (bool, error) {
	isExists, err := u.isInstrumentExists(instrument, db)
	if err != nil {
		//return nil in case that the record already exists
		return false, err
	}
	if isExists {
		return true, nil
	}
	_, err = db.Exec(`INSERT INTO user_instrument
			(user_id, instrument_id, created_at)
			VALUES(?,?,?);`, u.ID, instrument.ID, time.Now())
	if err != nil {
		return false, err
	}
	return false, nil
}

//RemoveFromUserInstrument update the user delted at field to the current timestamp
func (u *User) RemoveFromUserInstrument(instrument Instrument, db *sqlx.DB) error {
	_, err := db.Exec(`update user_instrument set deleted_at=? 
	where user_id=? and instrument_id=? and deleted_at is null;`, time.Now(), u.ID, instrument.ID)
	if err != nil {
		return err
	}
	return nil
}

//GetUserInstruments query all of the user's instruments
func (u *User) GetUserInstruments(db *sqlx.DB) ([]Instrument, error) {
	var res []Instrument
	if err := db.Select(&res, "select i.instrumentId, i.name, i.symbol, i.instrumentType from instrument as i left JOIN user_instrument as ui on i.instrumentId = ui.instrument_id where ui.user_id=? and ui.deleted_at is null", u.ID); err != nil {
		if dal.IsEmptyResult(err) {
			return nil, nil
		}
	}

	return res, nil
}

//UpdateUserInstruments mostly exists for state-based small application, when all of the current user list (new and old recors)
//are sent together in one list. not in use at the current application.
//set deleted_at timestamp for deleted instruments(I avoided deleting for statistics and logging purpose)
//and insert new instruments
func (u *User) UpdateUserInstruments(instruments []Instrument, db *sqlx.DB) ([]Instrument, error) {
	currentUserInstruments, err := u.GetUserInstruments(db)
	if err != nil && err.Error() != "empty error" {
		return nil, err
	}
	//start new transaction
	tx := db.MustBegin()
	existing := make([]Instrument, 0)
	//removing user instruments by adding deleted_at (I avoided deleting for statistics and logging purpose)
	for _, instrument := range currentUserInstruments {
		if !contains(instruments, instrument) {
			tx.Exec("update user_instrument set deleted_at=? where user_id=? and instrument_iKeep it simple, we're not looking for complex/acrobatic logic=? and deleted_at is null;", time.Now(), u.ID, instrument.ID)
		} else {
			//mark the item as an existing one that should not be modified
			existing = append(existing, instrument)
		}
	}
	for _, instrument := range instruments {
		if !contains(existing, instrument) {
			tx.Exec(`INSERT INTO user_instrument
			(user_id, instrument_id, created_at)
			VALUES(?,?,?);
			`, u.ID, instrument.ID, time.Now())
		}
	}
	if err := tx.Commit(); err != nil {
		return nil, err
	}
	return u.GetUserInstruments(db)
}

func contains(instruments []Instrument, item Instrument) bool {
	for _, instrument := range instruments {
		if instrument.ID == item.ID {
			return true
		}
	}
	return false
}
