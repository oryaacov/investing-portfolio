package dal

import (
	"context"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

//OpenDBConnection open a database using a connection string and ping the database
//to validate the connections (sqlx ping the database at the Connect function)
func OpenDBConnection(database, cs string) (*sqlx.DB, error) {
	ctx := context.Background()
	ctx, cancel := context.WithTimeout(ctx, time.Second*10)
	defer cancel()
	db, err := sqlx.ConnectContext(ctx, database, cs)
	if err != nil {
		return nil, err
	}
	return db, nil
}

func IsEmptyResult(err error) bool {
	return err.Error() == "sql: no rows in result set"
}
