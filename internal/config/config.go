package config

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"

	"bitbucket.org/oryaacov/investing-portfolio/pkg/logger"
)

// Configuration - env variables will be load from the docker-compose
type Configuration struct {
	App struct {
		Name       string `json:"name"`
		Enviroment string `json:"enviroment"`
		Version    string `json:"version"`
	} `json:"app"`
	DB struct {
		Port             int    `json:"port"`
		Database         string `json:"database"`
		ConnectionString string `json:"connectionString"`
	} `json:"db"`
	Log        logger.LogConfig `json:"log"`
	HTTPServer struct {
		Port                int    `json:"port"`
		BaseURL             string `json:"baseURL"`
		StaticFilesLocation string `json:"staticFilesLocation"`
		AllowedMethods      string `json:"allowedMethods"`
		AllowedHeaders      string `json:"allowedHeaders"`
		AllowedOrigins      string `json:"allowedOrigins"`
	} `json:"httpServer"`
}

//FromFile read the application configuration from a specific path
func FromFile(path string) (*Configuration, error) {
	var config Configuration
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return nil, errors.New("invalid file path")
	}
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	if jsnError := json.Unmarshal(bytes, &config); jsnError != nil {
		return nil, jsnError
	}
	return &config, nil
}
